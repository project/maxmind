<?php

namespace Drupal\Tests\maxmind_geoip\Unit\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\maxmind_geoip\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tests the Visitors Settings Form.
 *
 * @group maxmind_geoip
 */
class SettingsFormTest extends UnitTestCase {

  /**
   * Tests the buildForm() method.
   */
  public function testBuildForm() {
    $container = new ContainerBuilder();
    // Create a mock config object.
    $config = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $config->expects($this->exactly(2))
      ->method('get')
      ->willReturnMap([
        ['geoip_path', 'path/to/geoip/database'],
        ['license', 'secret-key-provided-by-maxmind'],
      ]);

    // Create a mock config factory object.
    $configFactory = $this->createMock('\Drupal\Core\Config\ConfigFactoryInterface');
    $configFactory->expects($this->once())
      ->method('get')
      ->with('maxmind_geoip.settings')
      ->willReturn($config);
    $container->set('config.factory', $configFactory);

    // Create a mock form state object.
    $formState = $this->createMock(FormStateInterface::class);

    $typed_config_manager = NULL;
    if (version_compare(\Drupal::VERSION, '10.2.0', '>=')) {
      $typed_config_manager = $this->createMock('\Drupal\Core\Config\TypedConfigManagerInterface');
      $container->set('config.typed', $typed_config_manager);
    }

    \Drupal::setContainer($container);
    // Create the form object.
    $form = SettingsForm::create($container);
    $form->setStringTranslation($this->getStringTranslationStub());

    // Build the form.
    $formArray = $form->buildForm([], $formState);

    // Assert that the form array contains the expected elements.
    $this->assertArrayHasKey('geoip_path', $formArray);
    $this->assertEquals('textfield', $formArray['geoip_path']['#type']);
    $this->assertEquals('GeoIP Database path', $formArray['geoip_path']['#title']);
    $this->assertEquals('path/to/geoip/database', $formArray['geoip_path']['#default_value']);

    $this->assertArrayHasKey('license', $formArray);
    $this->assertEquals('secret-key-provided-by-maxmind', $formArray['license']['#default_value']);
  }

  /**
   * Tests the submitForm() method.
   */
  public function testSubmitForm() {
    $container = new ContainerBuilder();
    // Create a mock editable config object.
    $editableConfig = $this->createMock('\Drupal\Core\Config\ImmutableConfig');
    $editableConfig->expects($this->exactly(2))
      ->method('set')
      ->with(
        $this->logicalOr(
          $this->equalTo('geoip_path'),
          $this->equalTo('license')
        ),
        $this->logicalOr(
          $this->equalTo('new/path/to/geoip/database'),
          $this->equalTo('secret-key-provided-by-maxmind')
        )
      )
      ->willReturnSelf();
    $editableConfig->expects($this->once())
      ->method('save');

    // Create a mock config factory object.
    $configFactory = $this->createMock('\Drupal\Core\Config\ConfigFactoryInterface');
    $configFactory->expects($this->once())
      ->method('getEditable')
      ->with('maxmind_geoip.settings')
      ->willReturn($editableConfig);
    $container->set('config.factory', $configFactory);

    $messenger = $this->createMock('\Drupal\Core\Messenger\MessengerInterface');
    $container->set('messenger', $messenger);

    // Create a mock form state object.
    $formState = $this->createMock(FormStateInterface::class);
    $formState->expects($this->once())
      ->method('getValues')
      ->willReturn([
        'geoip_path' => 'new/path/to/geoip/database',
        'license' => 'secret-key-provided-by-maxmind',
      ]);

    $typed_config_manager = NULL;
    if (version_compare(\Drupal::VERSION, '10.2.0', '>=')) {
      $typed_config_manager = $this->createMock('\Drupal\Core\Config\TypedConfigManagerInterface');
      $container->set('config.typed', $typed_config_manager);
    }
    \Drupal::setContainer($container);
    // Create the form object.
    $form = SettingsForm::create($container);
    $form->setStringTranslation($this->getStringTranslationStub());

    // Submit the form.
    $form_array = [
      'geoip_path' => [
        '#value' => 'new/path/to/geoip/database',
      ],
      'license' => [
        '#value' => 'secret-key-provided-by-maxmind',
      ],
    ];
    $form->submitForm($form_array, $formState);

    // Assert that the config was updated with the new value.
    $this->assertEquals('new/path/to/geoip/database', $form_array['geoip_path']['#value']);
    $this->assertEquals('secret-key-provided-by-maxmind', $form_array['license']['#value']);
  }

}
